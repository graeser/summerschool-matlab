T = 1
timesteps = 100

epsilon = 1e-2
epsilon = 1e-3
epsilon = 1e-4

beta1d = 1
beta2d = [1 1]'

N = 25
N = 50
N = 100



fd_transport(epsilon, beta1d, T, N, timesteps, 'dirichlet');
fd_transport(epsilon, beta1d, T, N, timesteps, 'neumann');

fe_stationary(25);
fe_transport(epsilon, beta2d, T, N, timesteps);



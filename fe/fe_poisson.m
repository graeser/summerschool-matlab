function [u, grid] = fe_poisson(grid, epsilon, f, uDirichlet)

disp('creating basis')
basis = lagrangeBasis(2,1);
n = basis.size(grid);


disp('assemble stiffness matrix');
A = globalOperatorAssembler(grid, basis, @localLaplaceAssembler, 8);


disp('assemble right hand side');
b = globalFunctionalAssembler(grid, basis, @(gg,ee,bb)localL2FunctionalAssembler(gg,ee,bb,f,1));

disp('incorporate boundary values');
for i=1:size(grid.nodes,2)
    if (grid.boundary(i))
        A(i,:) = 0;
        A(i,i) = 1;
        b(i) = uDirichlet(grid.nodes(:,i));
    end
end

disp('solve linear system');
u = A\b;


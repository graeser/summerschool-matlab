function Q = simplexQuadratureRule(dim, order)
% Returns a structure containing quadrature nodes and weights as well as their number.

    if ((dim==2) & (order<=1))
        points = [1/3  1/3]';
        weights = [0.5];
        order = 1;
    elseif ((dim==2) & (order<=2))
        % order 2 with 3 points by Hammer/Stroud
        r = 1/6;
        s = 4/6;
        points = [r r; r s; s r]';
        weights = [1/6 1/6 1/6];
        order = 2;
    elseif ((dim==2) & (order<=5))
        % order 5 with 7 points by Radon, Hammer/Marlowe/Stroud
        t = 1/3;
        r = (6-sqrt(15))/21;
        s = (9+2*sqrt(15))/21;
        u = (6+sqrt(15))/21;
        v = (9-2*sqrt(15))/21;
        points = [t t; r r; r s; s r; u u; u v; v u]';

        A = 9/40 * 0.5;
        B = (155-sqrt(15))/1200 * 0.5;
        C = (155+sqrt(15))/1200 * 0.5;
        weights = [A B B B C C C];

        order = 5;
    end

    Q = struct('size', length(weights), 'points', points, 'weights', weights, 'order', order);
end

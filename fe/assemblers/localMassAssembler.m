function A = localMassAssembler(grid,elementIndex,globalBasis)

dim = size(grid.nodes,1);
order = globalBasis.order(grid, elementIndex);
n = globalBasis.localSize(grid, elementIndex);

dT = elementTransformationJacobian(grid, elementIndex);
integrationElement = abs(det(dT));

% obtain quadrature rule of appropiate order
Q = simplexQuadratureRule(dim, order*2);

% evaluate basis functions at quadrature points
values = zeros(n, Q.size);
for i=1:n
    values(i,:) = globalBasis.evaluate(grid, elementIndex, i, Q.points);
end

A = zeros(n, n);
for k=1:Q.size
    weight = Q.weights(k)*integrationElement;

    % only compute lower triangle and diagonal entries
    for i=1:n
        z = values(i,k)*weight;
        for j=1:i
            A(i,j) = A(i,j) + values(j,k)*z;
        end
    end
end

% copy upper triangle (exploit symmetrie)
for i=1:n
    for j=(i+1):n
        A(i,j) = A(j,i);
    end
end

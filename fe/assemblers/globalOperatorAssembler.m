function matrix = globalOperatorAssembler(grid, basis, localAssembler, average_nnz)

% dimension of ansatz space
n = basis.size(grid);

% allocate matrix
matrix= spalloc(n,n,average_nnz*n);

% loop over all elements
for e=1:size(grid.elements,2)
    % compute local stiffness matrix
    localMatrix = localAssembler(grid, e, basis);

    % compute global indices
    indices = basis.index(grid, e);

    % add local matrix to corresponding global matrix entries
    matrix(indices,indices) = matrix(indices,indices) + localMatrix;
end

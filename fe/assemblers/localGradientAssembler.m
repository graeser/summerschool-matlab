function A = localGradientAssembler(grid,elementIndex,globalBasis, beta, beta_order)

dim = size(grid.nodes,1);
order = globalBasis.order(grid, elementIndex);
n = globalBasis.localSize(grid, elementIndex);

dT = elementTransformationJacobian(grid, elementIndex);
dT_inv = inv(dT);
integrationElement = abs(det(dT));

% obtain quadrature rule of appropiate order
Q = simplexQuadratureRule(dim, 2*order-1+beta_order);

% evaluate basis functions at quadrature points
values = zeros(n, Q.size);
for i=1:n
    values(i,:) = globalBasis.evaluate(grid, elementIndex, i, Q.points);
end

% evaluate gradients of basis functions at quadrature points
gradients = zeros(n, dim, Q.size);
for i=1:n
    gradients(i,:,:) = dT_inv' * globalBasis.evaluateJacobian(grid, elementIndex, i, Q.points);
end

% evaluate beta at quadrature points
if isa(beta, 'function_handle')
    % beta is either a globally defined function
    values_beta = beta(elementTransformation(grid, elementIndex, Q.points, dT));
elseif (size(beta,2) == size(grid.elements))
    % ... or a set a vectors given per element, i.e., a piecewise constant vector field
    values_beta = repmat(beta(:,elementIndex), 1, Q.size);
else
    % ... or a single vector, i.e., a constant vector field
    values_beta = repmat(beta, 1, Q.size);
end

A = zeros(n, n);
for k=1:Q.size
    weight = Q.weights(k)*integrationElement;

    for i=1:n
        z = dot(values_beta(:,k),gradients(i,:,k))*weight;
        for j=1:n
            A(i,j) = A(i,j) - values(j,k) * z;
        end
    end
end


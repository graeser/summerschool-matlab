function y = globalFunctionalAssembler(grid, basis, localFunctionalAssembler)

% dimension of ansatz space
n = basis.size(grid);

% allocate vector
y = zeros(n,1);


% loop over all elements
for e=1:size(grid.elements,2)
    % compute local stiffness matrix
    localB = localFunctionalAssembler(grid,e,basis);

    % compute global indices
    indices = basis.index(grid, e);

    % add local vector to corresponding global vector entries
    y(indices) = y(indices) + localB;
end

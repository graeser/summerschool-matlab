function y = localL2FunctionalAssembler(grid, elementIndex, globalBasis, f, quadOrder)

dim = size(grid.nodes,1);
order = globalBasis.order(grid, elementIndex);
n = globalBasis.localSize(grid, elementIndex);

dT = elementTransformationJacobian(grid, elementIndex);
integrationElement = abs(det(dT));

% obtain quadrature rule of appropiate order
Q = simplexQuadratureRule(dim, order+quadOrder);

% evaluate basis functions at quadrature points
values = zeros(n, Q.size);
for i=1:n
    values(i,:) = globalBasis.evaluate(grid, elementIndex, i, Q.points);
end

% evaluate f at quadrature points
values_f = f(elementTransformation(grid, elementIndex, Q.points, dT));

y = zeros(n,1);
for k =1:Q.size
    z = values_f(k)*Q.weights(k)*integrationElement;
    for i=1:n
        y(i) = y(i) + values(i,k)*z;
    end
end

end

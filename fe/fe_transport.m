function [u, grid] = fe_transport(epsilon, beta, T, N, timesteps, varargin)


disp('create grid');
grid = uniformGrid(N(1),N(end));

%grid = load('fe/grid/circle');
%grid.nodes = grid.nodes(1:2,:);
refinements = 0;
for i=1:refinements
    grid = refineGrid(grid);
end

disp('creating basis')
basis = lagrangeBasis(2,1);
n = basis.size(grid);



% initial value
u0 = @(x) ((x(1,:)-0.5).^2+(x(2,:)-.5).^2).^.5<.1;


tau = T/timesteps;

disp('assemble mass matrix');
MassMatrix = globalOperatorAssembler(grid, basis, @localMassAssembler, 8);

disp('assemble diffusion convection matrix');
DiffusionConvectionMatrix = globalOperatorAssembler(grid, basis, @(gg,ee,bb)localDiffusionConvectionAssembler(gg,ee,bb,epsilon,beta, 0), 8);

B = MassMatrix + tau*DiffusionConvectionMatrix;



for k=1:timesteps
    pause(.1);

    if (k==1)
        disp('assemble right hand side for first time step');
        rhs = globalFunctionalAssembler(grid, basis, @(gg,ee,bb)localL2FunctionalAssembler(gg,ee,bb,u0,1));
    else
        rhs = MassMatrix*u;
    end
    
    u = B\rhs;


    disp(['plot solution in timepstep k=' num2str(k)]);
    plotGridFunction(grid, basis, u);
    
    if (k==1)
        a=axis();
    end
%    axis(a);
end



%disp('incorporate boundary values');
%AA = A;
%for i=1:size(grid.nodes,2)
%    if (grid.boundary(i))
%        AA(i,:) = 0;
%        AA(i,i) = 1;
%        b(i) = 0;
%    end
%end




%vtk_trimesh(grid.elements, grid.nodes(1,:)', grid.nodes(2,:)', u, u);
%vtk_trisurf(grid.elements, grid.nodes(1,:)', grid.nodes(2,:)', u, u);




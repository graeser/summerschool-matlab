function plotGridFunction(grid, basis, u)

dim = size(grid.nodes,1);

if (dim==2)
%    referenceElement = generateGrid([0 0; 1 0; 0 1]', [1 2 3]');

    n_nodes = size(grid.nodes, 2);
    trisurf(grid.elements(1:3,:)', grid.nodes(1,:)', grid.nodes(2,:)', u(1:n_nodes), u(1:n_nodes));
end


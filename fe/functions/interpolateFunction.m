function u = interpolateFunction(grid, basis, varargin)

% dimension of ansatz space
n = basis.size(grid);

% initialize vector
u = zeros(n, 1);

% loop over all elements
for e=1:size(grid.elements,2)

    E = grid.nodes(1, grid.elements(:,e));

    %  transform function and derivatives to element to 
    fLocal = {};
    for i=1:length(varargin)
        fLocal{i} = @(x)varargin{i}(E(1) + (E(2)-E(1))*x);
    end

    % compute local interpolation
    uLocal = basis.localInterpolation(grid, e, fLocal{:});

    % local size
    nLocal = basis.localSize(grid, e);

    % compute global indices
    for j=1:nLocal
        u(basis.index(grid, e, j)) = uLocal(j);
    end
end

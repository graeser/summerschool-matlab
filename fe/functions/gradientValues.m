function du = gradientValues(grid, basis, u)

dim = size(grid.nodes,1);

% center of reference simplex
center = repmat(1/(dim+1),dim,1);


% allocate matrix
du= zeros(dim, size(grid.elements,2));

% loop over all elements
for e=1:size(grid.elements,2)
    % compute geometry information
    simplex = grid.nodes(:,grid.elements(1:(dim+1),e));
    dT = zeros(dim);
    for i=1:dim
        dT(:,i) = simplex(:,i+1) - simplex(:,1);
    end
    integrationElement = abs(det(dT));
    dTInvTrans = inv(dT');
    
    % compute global indices
    indices = basis.index(grid, e);

    du(:,e) = 0;
    for i=1:globalBasis.localSize(grid, e)
        gradient_i = dTInvTrans * globalBasis.evaluateJacobian(grid, e, i, center);
        du(:,e) = du(:,e) + u(indices(i))* gradient_i;
    end
end

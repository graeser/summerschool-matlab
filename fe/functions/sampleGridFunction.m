function [xx,yy] = sampleGridFunction(grid, basis, u, localPoints)

dim = size(grid.nodes,1);

% store all sampled values and produce one plot only
xx = zeros(length(localPoints), size(grid.elements,2));
yy = zeros(length(localPoints), size(grid.elements,2));

for e=1:size(grid.elements,2)
    vertices = grid.elements(1:(dim+1),e);
    element = grid.nodes(vertices);

    xx(:,e) = linspace(element(1), element(2), length(localPoints));
    for i=1:basis.localSize(grid, e)
        globalIndex = basis.index(grid, e, i);
        values = basis.evaluate(grid, e, i, localPoints);
        yy(:,e) = yy(:,e) + (u(globalIndex) * values(:));
    end
end

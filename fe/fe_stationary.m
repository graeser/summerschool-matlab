function [u, grid] = fe_stationary(N)

% data sets
f = @(x) 10*ones(1,size(x,2));
uDirichlet = @(x) x(1,:)*0;

f = @(x) x(1,:)*0;
uDirichlet = @(x) sin(x(2,:)*pi)*(1+x(1,:));



disp('create grid');
grid = uniformGrid(N(1),N(end));

%grid = load('circle');
refinements = 0;
for i=1:refinements
    grid = refineGrid(grid);
end

disp('creating basis')
basis = lagrangeBasis(2,1);
n = basis.size(grid);

u = fe_poisson(grid, 1, f, uDirichlet);


disp('plot solution');
plotGridFunction(grid, basis, u);
%vtk_trimesh(grid.elements, grid.nodes(1,:)', grid.nodes(2,:)', u, u);
%vtk_trisurf(grid.elements, grid.nodes(1,:)', grid.nodes(2,:)', u, u);




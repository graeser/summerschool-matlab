function B = lagrangeBasis(dim, order)

    if ((order==1) & (dim==1))
        B = struct( ...
            'dim', 1, ...
            'evaluate', @D1P1Evaluate, ...
            'evaluateJacobian', @D1P1EvaluateJacobian, ...
            'localSize', @(grid, elementIndex)(2), ...
            'localInterpolation', @P1LocalInterpolation, ...
            'order', @(grid, elementIndex)(1),...
            'index', @P1Index,...
            'size', @(grid)(size(grid.nodes,2)));
    elseif ((order==1) & (dim==2))
        B = struct( ...
            'dim', 2, ...
            'evaluate', @D2P1Evaluate, ...
            'evaluateJacobian', @D2P1EvaluateJacobian, ...
            'localSize', @(grid, elementIndex)(3), ...
            'localInterpolation', @P1LocalInterpolation, ...
            'order', @(grid, elementIndex)(1),...
            'index', @P1Index,...
            'size', @(grid)(size(grid.nodes,2)));
    end

end



function u = P1LocalInterpolation(grid, elementIndex, f, df, ddf, dddf)
    element = grid.nodes(grid.elements(:,elementIndex));
    n = size(element,2);
    u = zeros(1,n);
    for i=1:n
        u(i) = f(element(:,i));
    end
end

function globalIndices = P1Index(grid, elementIndex, localIndices)
% Compute global indices of basis functions with given localIndices in element.
    dim = size(grid.nodes,1);
    globalIndices = grid.elements(1:(dim+1), elementIndex);
    if (nargin==3)
        globalIndices = globalIndices(localIndices);
    end
end

function y = D1P1Evaluate(grid, elementIndex, localIndex, x)
    switch localIndex
        case 1,
            y = x;
        case 2,
            y = 1-x;
    end
end

function Dx = D1P1EvaluateJacobian(grid, elementIndex, localIndex, x)
    Dx = x*0;
    switch localIndex
        case 1,
            Dx(1,:) = 1;
        case 2 ,
            Dx(1,:) = -1;
    end
end


function y = D2P1Evaluate(grid, elementIndex, localIndex, x)
    switch localIndex
        case 1,
            y = 1 - x(1,:) - x(2,:);
        case 2 ,
            y = x(1,:);
        case 3,
            y = x(2,:);
    end
end

function Dx = D2P1EvaluateJacobian(grid, elementIndex, localIndex, x)
    Dx = x*0;
    switch localIndex
        case 1,
            Dx(1,:) = -1;
            Dx(2,:) = -1;
        case 2 ,
            Dx(1,:) = 1;
            Dx(2,:) = 0;
        case 3,
            Dx(1,:) = 0;
            Dx(2,:) = 1;
    end
end




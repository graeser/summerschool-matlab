function T = assemble_transfer_operator(coarse_grid, fine_grid)


n_coarse_nodes = size(coarse_grid.nodes, 2);
n_fine_nodes = size(fine_grid.nodes, 2);
n_fine_elements = size(fine_grid.elements, 2);

T = spalloc(n_fine_nodes, n_coarse_nodes, 3*n_coarse_nodes);

for k = 1:n_fine_elements
end

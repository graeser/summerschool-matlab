function h = elementSize(grid, elementIndex)

dim = size(grid.nodes,1);
simplex = grid.nodes(:,grid.elements(1:(dim+1),elementIndex));

h = 0;
for i=1:(dim+1)
    for j=(i+1):(dim+1)
        h = max(h, norm(simplex(:,i)-simplex(:,j)));
    end
end

function xGlobal = elementTransformation(grid, elementIndex, xLocal, dT)

firstVertex = grid.nodes(:,grid.elements(1,elementIndex));

xGlobal = xLocal*0;
for i=1:size(xLocal,2)
    xGlobal(:,i) = firstVertex + dT*xLocal(:,i);
end

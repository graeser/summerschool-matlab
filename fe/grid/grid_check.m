function grid_check(grid)
% check grid for consistency

n_elements = size(grid.elements,2);
for k = 1:n_elements
    element_nodes = grid.elements(1:3,k);
    edge_nodes = grid.edges(1:2,grid.elements(4:6,k));
    edge_nodes = edge_nodes(:);

    for i=1:length(edge_nodes)
        if (length(find(element_nodes==edge_nodes(i)))<0)
            disp('To many nodes in edge');
        end
    end
    for i=1:length(element_nodes)
        if (length(find(edge_nodes==element_nodes(i)))<0)
            disp('To few nodes in edge');
        end
    end
end

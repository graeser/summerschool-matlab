function xLocal = elementTransformationInverse(grid, elementIndex, xGlobal, dT_inv)

firstVertex = grid.nodes(:,grid.elements(1,elementIndex));

xLocal = xGlobal*0;
for i=1:size(xLocal,2)
    xLocal(:,i) = dT_inv*(xGlobal(:,i) - firstVertex);
end

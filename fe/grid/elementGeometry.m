function geo = elementGeometry(grid, elementIndex)

dim = size(grid.nodes,1);
vertices = grid.nodes(:,grid.elements(1:(dim+1),elementIndex));

dT = zeros(dim);
for i=1:dim
    dT(:,i) = vertices(:,i+1) - vertices(:,1);
end

geo = struct( ...
        'dim', dim, ...
        'vertices', vertices, ...
        'jacobian', dT,
        'inverseJacobian', inv(dT),
        'global', @(x)D1P1EvaluateJacobian, ...
        'localSize', @(grid, elementIndex)(2), ...
        'localInterpolation', @P1LocalInterpolation, ...


integrationElement = abs(det(dT));
dTInvTrans = inv(dT');



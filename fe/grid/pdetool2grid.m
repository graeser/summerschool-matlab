function grid = pdetool2grid(p,e,t)
% generate grid structure from matlab pdetool-triple

grid = generate_grid(p, t(1:3,:));


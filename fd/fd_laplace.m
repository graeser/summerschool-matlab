function A = fd_laplace(n)

h = 1/(n-1);

diagonals=ones(n,3);
diagonals(:,1) = 1/(h^2);
diagonals(:,2) = -2/(h^2);
diagonals(:,3) = 1/(h^2);

A = spdiags(diagonals, [-1 0 1], n, n);

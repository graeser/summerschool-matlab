function A = fd_gradient(n)

h = 1/(n-1);

diagonals=ones(n,2);
diagonals(:,1) = -1/(2*h);
diagonals(:,2) = +1/(2*h);

A = spdiags(diagonals, [-1 1], n, n);
